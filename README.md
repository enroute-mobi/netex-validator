# Netex::Validator

Welcome to your new gem! In this directory, you'll find the files you need to be able to package up your Ruby library into a gem. Put your Ruby code in the file `lib/netex/validator`. To experiment with that code, run `bin/console` for an interactive prompt.

TODO: Delete this and the text above, and describe your gem

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'netex-validator'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install netex-validator

## Usage

```
# Validate NeTEx file with European Profile
bundle exec ruby exe/netex-validate --rule-set European netex.zip

# Validate NeTEx file with IDFM Profile for Line
bundle exec ruby exe/netex-validate --rule-set IDFM::Line netex.zip

# Validate NeTEx file with all IDFM Profile
bundle exec ruby exe/netex-validate --rule-set IDFM::Full netex.zip
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and the created tag, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/netex-validator.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
