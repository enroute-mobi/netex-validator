RSpec.describe Netex::Validator::Base do
  subject(:validator) { described_class.new }

  describe '.load' do
    context 'when a Ruleset class is given (like Netex::Validator::RuleSet::European)' do
      let(:ruleset) { Netex::Validator::RuleSet::European }

      it do
        expect(validator.checker).to receive(:load).with(an_instance_of(ruleset))
        validator.load ruleset
      end
    end

    context 'when a Ruleset instance is given' do
      let(:ruleset) { Netex::Validator::RuleSet::Base.new }

      it do
        expect(validator.checker).to receive(:load).with(ruleset)
        validator.load ruleset
      end
    end
  end
end
