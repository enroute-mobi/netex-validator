# frozen_string_literal: true

RSpec.describe Netex::Validator::Checker do
  subject(:checker) { described_class.new }

  describe '#messages' do
    subject { checker.messages }

    context 'when rules have messages' do
      let(:rule) { Netex::Validator::Rule::Base.new }
      let!(:message) { rule.create_message message: 'test' }

      before do
        checker.rules << rule
      end

      it { is_expected.not_to be_empty }

      it 'returns messages of all rules' do
        expect(subject).to contain_exactly(message)
      end
    end

    context 'when no message is present' do
      it { is_expected.to be_empty }
    end
  end
end
