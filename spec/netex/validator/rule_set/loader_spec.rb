# frozen_string_literal: true

# loader_spec.rb

RSpec.describe Netex::Validator::RuleSet::Loader do
  subject(:loader) { described_class.new }

  describe '#parse' do
    self::SAMPLE_1 = <<~JSON
      [
          {
              "rule": "attribute/mandatory",
              "name": "longitude",
              "criticity": "error",
              "code": "longitude-mandatory",
              "message": "Una parada debe estar ubicada geográficamente Indicar una longitud"
          }
      ]
    JSON

    context "with JSON '#{self::SAMPLE_1}'" do
      let(:json) { self.class::SAMPLE_1 }

      it {
        expect do
          loader.parse(json)
        end.to change(loader, :descriptions).from(be_empty).to(an_object_having_attributes(size: 1))
      }
    end

    self::SAMPLE_2 = <<~JSON
      [
          {
              "rule": "reference/exists",
              "referenced_classes": ["Quay", "StopPlace", "OperationPeriod"],
              "criticity": "warning",
              "code": "unknown-reference",
              "tags": [ "next" ]
          }
      ]
    JSON

    context "with JSON '#{self::SAMPLE_2}'" do
      let(:json) { self.class::SAMPLE_2 }

      it {
        expect do
          loader.parse(json)
        end.to change(loader, :descriptions).from(be_empty).to(an_object_having_attributes(size: 1))
      }
    end
  end

  describe Netex::Validator::RuleSet::Loader::Base do
    subject(:base) { described_class.new }

    describe '#prepared_options' do
      subject { base.prepared_options }

      context 'with option dummy: "/abc/"' do
        before { base.options[:dummy] = '/abc/' }

        it { is_expected.to include(dummy: /abc/) }
      end

      context 'with option dummy_class: "StopPlace"' do
        before { base.options[:dummy_class] = 'StopPlace' }

        it { is_expected.to include(dummy_class: Netex::StopPlace) }
      end

      context 'with option dummy_class: "StopPlace"' do
        before { base.options[:dummy_classes] = ['StopPlace'] }

        it { is_expected.to include(dummy_classes: [Netex::StopPlace]) }
      end
    end
  end

  describe Netex::Validator::RuleSet::Loader::Rule do
    subject(:rule) { described_class.new }

    describe '#rule_class' do
      subject { rule.rule_class }

      context "when rule is 'attribute/mandatory'" do
        before { rule.rule = 'attribute/mandatory' }

        it { is_expected.to eq(Netex::Validator::Rule::Attribute::Mandatory) }
      end
    end

    describe '#to_rule' do
      subject { rule.to_rule }

      context "rule_class is ..::Rule::Attribute::Mandatory and rule_attributes { name: 'dummy' }" do
        before do
          allow(rule).to receive_messages(rule_class: Netex::Validator::Rule::Attribute::Mandatory,
                                          rule_attributes: { name: 'dummy' })
        end

        it { is_expected.to be_a(Netex::Validator::Rule::Attribute::Mandatory) }

        it { is_expected.to have_attributes(name: 'dummy') }
      end
    end
  end
end
