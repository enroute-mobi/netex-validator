# frozen_string_literal: true

# base_spec.rb

RSpec.describe Netex::Validator::RuleSet::Base do
  subject(:ruleset) { described_class.new }

  describe '#empty?' do
    context 'when no rule is defined' do
      it { is_expected.to be_empty }
    end

    context 'when a rule is added' do
      before { ruleset.rules << double }

      it { is_expected.not_to be_empty }
    end
  end
end
