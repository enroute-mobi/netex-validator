RSpec.describe Netex::Validator::Rule do

  describe '.from_name' do
    subject { described_class.from_name(name) }

    context 'when given name is nil' do
      let(:name) { nil }
      it { is_expected.to be_nil }
    end

    context 'when given name is ""' do
      let(:name) { 'dummy' }
      it { is_expected.to be_nil }
    end

    context 'when given name is "resource/mandatory"' do
      let(:name) { 'resource/mandatory' }

      it { is_expected.to eq(Netex::Validator::Rule::Resource::Mandatory) }
    end
  end

  describe Netex::Validator::Rule::Collection::Count do
    subject(:rule) { described_class.new }

    describe '#visit' do
      context 'when Rule checks key_list collection' do
        before { rule.collection = :key_list }

        context 'when minimum_count is 1' do
          before { rule.minimum_count = 1 }

          context 'when resource has no key list' do
            let(:resource) { Netex::Line.new(key_list: []) }

            it { expect { rule.visit(resource) }.to change(rule, :messages).from(be_empty).to(be_one) }

            describe 'messages' do
              subject { rule.messages }

              before { rule.visit(resource) }

              let(:message) do
                'Collection key_list contains less elements than expected (0 < 1)'
              end

              it { is_expected.to include(an_object_having_attributes(message: message)) }
            end
          end

          context 'when resource has one key list' do
            let(:resource) { Netex::Line.new(key_list: [Netex::KeyValue.new(key: 'dummy')]) }

            it { expect { rule.visit(resource) }.not_to change(rule, :messages).from(be_empty) }
          end
        end

        context 'when maximum_count is 0' do
          before { rule.maximum_count = 0 }

          context 'when resource has one key list' do
            let(:resource) { Netex::Line.new(key_list: [Netex::KeyValue.new(key: 'dummy')]) }

            it { expect { rule.visit(resource) }.to change(rule, :messages).from(be_empty).to(be_one) }

            describe 'messages' do
              subject { rule.messages }

              before { rule.visit(resource) }

              let(:message) do
                'Collection key_list contains more elements than expected (1 > 0)'
              end

              it { is_expected.to include(an_object_having_attributes(message: message)) }
            end
          end

          context 'when resource has no key list' do
            let(:resource) { Netex::Line.new(key_list: []) }

            it { expect { rule.visit(resource) }.not_to change(rule, :messages).from(be_empty) }
          end
        end
      end
    end
  end

  describe Netex::Validator::Rule::Attribute::Mandatory do
    subject(:rule) { described_class.new }

    describe '#visit' do
      context 'when Rule checks name attribute' do
        before { rule.name = :name }

        [nil, '', '  '].each do |value|
          context "when resource name is #{value.inspect}" do
            let(:resource) { Netex::Line.new(name: value) }

            it { expect { rule.visit(resource) }.to change(rule, :messages).from(be_empty).to(be_one) }

            describe 'messages' do
              subject { rule.messages }

              before { rule.visit(resource) }

              let(:message) do
                'Mandatory attribute name not found'
              end

              it { is_expected.to include(an_object_having_attributes(message: message)) }
            end
          end
        end

        context "when resource name is 'dummy'" do
          let(:resource) { Netex::Line.new(name: 'dummy') }

          it { expect { rule.visit(resource) }.not_to change(rule, :messages).from(be_empty) }
        end
      end
    end
  end

  describe Netex::Validator::Rule::Attribute::Blank do
    subject(:rule) { described_class.new }

    describe '#visit' do
      context 'when Rule checks name attribute' do
        before { rule.name = :name }

        context "when resource name is 'dummy'" do
          let(:resource) { Netex::Line.new(name: 'dummy') }

          it { expect { rule.visit(resource) }.to change(rule, :messages).from(be_empty).to(be_one) }

          describe 'messages' do
            subject { rule.messages }

            before { rule.visit(resource) }

            let(:message) do
              'Blank attribute name found'
            end

            it { is_expected.to include(an_object_having_attributes(message: message)) }
          end
        end

        [nil, '', '  '].each do |value|
          context "when resource name is #{value.inspect}" do
            let(:resource) { Netex::Line.new(name: value) }

            it { expect { rule.visit(resource) }.not_to change(rule, :messages).from(be_empty) }
          end
        end
      end
    end
  end

  describe Netex::Validator::Rule::ServiceJourney::PassingTimesChronology do
    subject(:rule) { described_class.new }

    describe '#visit' do
      let(:resource) { Netex::ServiceJourney.new }

      context 'when a TimetabledPassingTime has a departure time before its arrival time' do
        before do
          resource.passing_times << Netex::TimetabledPassingTime.new(id: 'wrong', arrival_time: '14:01',
                                                                     departure_time: '14:00')
        end

        it { expect { rule.visit(resource) }.to change(rule, :messages).from(be_empty).to(be_one) }

        describe 'messages' do
          subject { rule.messages }

          before { rule.visit(resource) }

          let(:message) { 'Departure time is before arrival time' }

          it { is_expected.to include(an_object_having_attributes(message: message, resource_id: 'wrong')) }
        end
      end

      context 'when a TimetabledPassingTime has a arrival time before the previous departure time' do
        before do
          resource.passing_times << Netex::TimetabledPassingTime.new(departure_time: '14:00')
          resource.passing_times << Netex::TimetabledPassingTime.new(id: 'wrong', arrival_time: '13:59')
        end

        it { expect { rule.visit(resource) }.to change(rule, :messages).from(be_empty).to(be_one) }

        describe 'messages' do
          subject { rule.messages }

          before { rule.visit(resource) }

          let(:message) { 'Arrival time is before previous departure time' }

          it { is_expected.to include(an_object_having_attributes(message: message, resource_id: 'wrong')) }
        end
      end
    end
  end
end
