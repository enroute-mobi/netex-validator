RSpec.describe Netex::Validator::RuleContext do
  describe '.from_name' do
    subject { described_class.from_name(name) }

    context 'when given name is nil' do
      let(:name) { nil }
      it { is_expected.to be_nil }
    end

    context 'when given name is ""' do
      let(:name) { 'dummy' }
      it { is_expected.to be_nil }
    end

    context 'when given name is "resource/kind_of"' do
      let(:name) { 'resource/kind_of' }

      it { is_expected.to eq(Netex::Validator::RuleContext::Resource::KindOf) }
    end
  end
end

RSpec.describe Netex::Validator::RuleContext::Base do
  subject(:rule_context) { described_class.new.tap { |c| c.rules << rule } }

  let(:rule) { double visit: true, match?: true }


  describe Netex::Validator::RuleContext::Resource::KindOf do
    describe 'when resource class is Netex::StopPlace' do
      before { rule_context.resource_class = Netex::StopPlace }

      context 'when resource is a Netex::StopPlace' do
        let(:resource) { Netex::StopPlace.new }

        it do
          expect(rule).to receive(:visit).with(resource, any_args)
          rule_context.visit resource
        end
      end

      context 'when resource is a Netex::Line' do
        let(:resource) { Netex::Line.new }

        it do
          expect(rule).not_to receive(:visit)
          rule_context.visit resource
        end
      end
    end
  end

  describe Netex::Validator::RuleContext::Reduce::Collection do
    context 'when collection is key_list' do
      before { rule_context.collection = :key_list }

      context 'when resource has a one key list' do
        let(:resource) { Netex::Line.new(key_list: [key_value]) }

        let(:key_value) { Netex::KeyValue.new(key: 'dummy') }

        it do
          expect(rule).to receive(:visit).with(key_value, any_args)
          rule_context.visit resource
        end
      end
    end
  end

  describe Netex::Validator::RuleContext::Reduce::Element do
    context 'when element is presentation' do
      before { rule_context.element = :presentation }

      context 'when resource is Line with a presentation' do
        let(:resource) { Netex::Line.new(presentation: element) }

        let(:element) { Netex::Presentation.new }

        it do
          expect(rule).to receive(:visit).with(element, any_args)
          rule_context.visit resource
        end
      end
    end
  end
end
