# frozen_string_literal: true

require 'optparse'
require 'netex/validator'

module Netex
  module Validator
    # Setup and start a Validator according to command line
    class CLI
      def rule_sets
        @rule_sets ||= []
      end

      def targets
        @targets ||= []
      end

      def ignored_tags
        @ignored_tags ||= []
      end

      def option_parser
        OptionParser.new do |opts|
          opts.banner = 'Usage: netex-validate [options] [files or URLs]'

          opts.on('-r', '--rule-set RULESET', 'Use the rules from the specified rule set') do |rule_set|
            rule_sets << rule_set
          end

          opts.on('-i', '--ignore-tag TAG', 'Ignore the rules with the specified tag') do |tag|
            ignored_tags << tag
          end

          # opts.on("-c", "--color", "Enable syntax highlighting") do
          #   @options[:syntax_highlighting] = true
          # end
        end
      end

      def parse
        option_parser.parse!(arguments)
        targets.concat arguments
      end

      def arguments
        ARGV
      end

      def validator
        @validator ||= Netex::Validator::Base.new(ignored_tags: ignored_tags).tap do |validator|
          rule_sets.each do |rule_set_name|
            rule_set = RuleSet.from_name(rule_set_name)
            rule_set ||= Netex::Validator::RuleSet::Loader.load(URI.open(rule_set_name))
            
            validator.load rule_set
          end
        end
      end

      def validate
        targets.each do |target|
          validator.validate URI.open(target)
        end
      end

      def describe
        validator.describe(Description::Output.new)
        puts "#{validator.resource_count} resources processed in #{validator.duration.round(3)}s"
      end

      def self.start
        cli = new
        cli.parse
        cli.validate
        cli.describe
      end
    end
  end
end
