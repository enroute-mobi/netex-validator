# frozen_string_literal: true

module Netex
  module Validator
    module RuleSet
      def self.from_name(name)
        const_get name
      rescue NameError
        nil
      end

      # Base class for all RuleSets
      class Base
        def rules
          @rules ||= []
        end

        def empty?
          rules.empty?
        end
      end
    end
  end
end
