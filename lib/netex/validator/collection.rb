# frozen_string_literal: true

module Netex
  module Validator
    class Collection
      include MutableRuler

      attr_accessor :collection

      def initialize(collection: nil)
        @collection = collection
      end

      def add(rule)
        rules << rule
        self
      end
      alias << add

      def match?(_resource)
        true
      end

      def visit(resource, options = {})
        resource.send(collection).each_with_index do |element, index|
          super element, options.merge(index: index)
        end
      end

      def describe(d)
        super d.context(description)
      end

      def description
        "In collection #{collection}"
      end
    end
  end
end
