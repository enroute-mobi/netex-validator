# frozen_string_literal: true

module Netex
  module Validator
    # Validates a given NeTEx file
    class Base
      def initialize(ignored_tags: nil)
        @ignored_tags = ignored_tags || []
        @resource_count = 0
      end

      attr_reader :ignored_tags, :resource_count, :duration

      def checker
        @checker ||= Checker.new(ignored_tags: ignored_tags)
      end

      def load(rule_set)
        if rule_set.is_a?(Class) && rule_set < RuleSet::Base
          rule_set = rule_set.new
        end

        checker.load rule_set
      end

      def validate(file)
        @duration = Benchmark.realtime do
          unless checker.empty?
            Netex::Source.read(file) do |source|
              source.resources.each do |resource|
                @resource_count += 1
                checker.visit resource
              end
            end

            checker.finalize
          end
        end
      end

      def describe(description)
        checker.describe(description)
      end

      def messages
        checker.messages
      end
    end
  end
end
