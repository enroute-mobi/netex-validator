# frozen_string_literal: true

module Netex
  module Validator
    # Check a given set of Rules
    class Checker
      include Ruler

      def initialize(options = {})
        options.each { |k, v| send "#{k}=", v }
      end

      def visit(resource)
        super resource, ignored_tags: ignored_tags
      end

      def load(rule_set)
        rule_set.rules.each do |rule|
          rules << rule
        end
      end

      def ignored_tags
        @ignored_tags ||= []
      end
      attr_accessor :ignored_tags
    end
  end
end
