# frozen_string_literal: true

module Netex
  module Validator
    module RuleContext

      def self.from_name(name)
        # TODO: add cache
        candidate_class = Object.const_get("::#{self.name}::#{name&.classify}")
        return nil unless candidate_class < Base

        candidate_class
      rescue NameError
        nil
      end

      # Base class for all RuleContexts
      class Base
        include MutableRuler

        def initialize(attributes = {})
          attributes.each { |k, v| send "#{k}=", v }
        end

        def describe(d)
          super d.context(description)
        end

        def match?(_resource)
          true
        end
      end

      module Resource
        class KindOf < Base
          attr_accessor :resource_classes

          def resource_class=(resource_class)
            self.resource_classes = [resource_class]
          end

          def match?(resource)
            resource_classes.any? do |resource_class|
              resource.is_a?(resource_class)
            end
          end

          def visit(resource, options = {})
            return unless match?(resource)
            super
          end

          def description
            "Resource is kind of #{resource_classes.join(',')}"
          end
        end
      end

      module Attribute
        class Match < Base
          attr_accessor :name, :value

          def match?(resource)
            resource.send(name).match?(value)
          end

          def description
            "Attribute #{name} matches #{value}"
          end
        end
      end

      module Reduce
        class Collection < Base
          include MutableRuler

          attr_accessor :collection

          def match?(resource)
            resource.respond_to?(collection)
          end

          def visit(resource, options = {})
            resource.send(collection).each_with_index do |element, index|
              super element, options.merge(index: index)
            end
          end

          def description
            "In collection #{collection}"
          end
        end

        class Element < Base
          include MutableRuler

          attr_accessor :element

          def match?(resource)
            resource.respond_to?(element)
          end

          def visit(resource, options = {})
            resource_element = resource.send(element)
            super resource_element, options.merge(parent_id: resource.id)
          end

          def description
            "In element #{element}"
          end
        end
      end
    end
  end
end
