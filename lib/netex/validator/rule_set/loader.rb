# frozen_string_literal: true

module Netex
  module Validator
    module RuleSet
      class Loader
        def self.load(definition)
          new.parse(definition).load
        end

        def deserialize(object)
          if object.is_a?(Hash)
            return Rule.new(object) if object['rule']
            return RuleContext.new(object) if object['rule_context']
          end

          object
        end

        class Base
          def initialize(attributes = {})
            attributes.each do |k, v|
              writer_method = "#{k}="

              if respond_to?(writer_method)
                send writer_method, v
              else
                options[k.to_sym] = v
              end
            end
          end

          def options
            @options ||= {}
          end

          def prepared_options
            options.map do |attribute, value|
              value =
                if attribute.end_with?('_class')
                  netex_resource_class(value)
                elsif attribute.end_with?('_classes')
                  Array(value).map { |v| netex_resource_class(v) }
                else
                  value
                end

              value = Regexp.new ::Regexp.last_match(1) if value.is_a?(String) && %r{^/(.*)/$} =~ value

              [attribute, value]
            end.to_h
          end

          def netex_resource_class(name)
            Netex::Resource.resource_class name
          end
        end

        class Rule < Base
          attr_accessor :rule, :criticity, :code, :message, :tags

          def rule_class
            Netex::Validator::Rule.from_name(rule)
          end

          def rule_attributes
            {
              criticity: criticity,
              code: code,
              message: message
            }.merge(prepared_options).compact
          end

          def to_rule
            rule_class.new rule_attributes
          end
        end

        class RuleContext < Base
          attr_accessor :rule_context

          def rule_context_class
            Netex::Validator::RuleContext.from_name(rule_context)
          end

          def rules
            @rules ||= []
          end

          attr_writer :rules

          def rule_context_attributes
            prepared_options
          end

          def to_rule
            rule_context_class.new(rule_context_attributes).tap do |rule_context|
              rules.each { |rule| rule_context << rule.to_rule }
            end
          end
        end

        def parse(definition)
          descriptions.concat JSON.load(
            definition,
            lambda { |object|
              case object
              when Hash
                object.transform_values { |value| deserialize value }
              when Array
                object.map! { |value| deserialize value }
              end
            }
          )

          self
        end

        def load
          descriptions.each do |description|
            rules << description.to_rule
          end

          self
        end

        def descriptions
          @descriptions ||= []
        end

        def rules
          @rules ||= []
        end
      end
    end
  end
end
