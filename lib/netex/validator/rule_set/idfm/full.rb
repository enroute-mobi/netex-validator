# frozen_string_literal: true

# lib/full.rb

module Netex
  module Validator
    module RuleSet
      module IDFM
        class Full < Netex::Validator::RuleSet::Base # rubocop:disable Metrics/ClassLength
          def initialize # rubocop:disable Metrics/MethodLength
            super

            rules << calendrier_frame_rule
            rules << offre_frame_rule
            rules << data_source_ref_rule
            rules << route_direction_type_rule
            rules << service_journey_pattern_rule
            rules << line_operator_data_source_ref_rule

            [Netex::DayType, Netex::Operator, Netex::ScheduledStopPoint, Netex::Direction, Netex::DestinationDisplay,
             Netex::Route].each do |resource_class|
              rules << ::Netex::Validator::Rule::Reference::Exists.new(referenced_class: resource_class)
            end

            rules << ::Netex::Validator::Rule::Reference::Version.new(
              value: 'any', referenced_classes: [Netex::Line, Netex::Quay, Netex::StopPlace]
            )

            rules << ::Netex::Validator::Rule::Reference::Exists.new(
              referenced_class: Netex::Line
            )
            # TODO: CHOUETTE-1641 Incompatible with Quays exported as StopPlace TypeOfPlaceRef quay
            # rules << ::Netex::Validator::Rule::Reference::Exists.new(
            #   referenced_class: Netex::Quay
            # )
            rules << ::Netex::Validator::Rule::Reference::Exists.new(
              referenced_class: Netex::StopPlace
            )
          end

          def calendrier_frame_rule # rubocop:disable Metrics/MethodLength
            ::Netex::Validator::RuleContext::Resource::KindOf.new(
              resource_class: Netex::GeneralFrame
            ).add(
              ::Netex::Validator::RuleContext::Attribute::Match.new(
                name: 'id', value: 'CALENDRIER'
              ).add(
                ::Netex::Validator::Rule::Resource::Mandatory.new
              ).add(
                ::Netex::Validator::Rule::Attribute::Mandatory.new(
                  name: 'valid_between'
                )
              )
            )
          end

          def offre_frame_rule # rubocop:disable Metrics/MethodLength
            ::Netex::Validator::RuleContext::Resource::KindOf.new(
              resource_class: Netex::CompositeFrame
            ).add(
              ::Netex::Validator::RuleContext::Attribute::Match.new(
                name: 'id',
                value: 'NETEX_OFFRE_LIGNE'
              ).add(
                ::Netex::Validator::Rule::Attribute::Match.new(
                  name: 'id',
                  value: /FR1:CompositeFrame:NETEX_OFFRE_LIGNE-\d{8}T\d{6}Z:LOC/
                )
              ).add(
                ::Netex::Validator::Rule::Attribute::Value.new(
                  name: 'data_source_ref',
                  value: 'FR1-OFFRE_AUTO'
                )
              ).add(
                ::Netex::Validator::Rule::Attribute::Mandatory.new(
                  name: 'name'
                )
              )
            )
          end

          def data_source_ref_rule # rubocop:disable Metrics/MethodLength
            ::Netex::Validator::RuleContext::Resource::KindOf.new(
              resource_classes: [
                Netex::Quay,
                Netex::StopPlace,
                Netex::DayType,
                Netex::DayTypeAssignment,
                Netex::DestinationDisplay,
                Netex::Direction,
                Netex::Line,
                Netex::Operator,
                Netex::OperatingPeriod,
                Netex::PassengerStopAssignment,
                Netex::Route,
                Netex::RoutePoint,
                Netex::ScheduledStopPoint,
                Netex::ServiceJourney,
                Netex::ServiceJourneyPattern
              ]
            ).add(
              ::Netex::Validator::Rule::Attribute::Mandatory.new(
                name: 'data_source_ref',
                tag: 'data_source_ref'
              )
            )
          end

          def route_direction_type_rule
            ::Netex::Validator::RuleContext::Resource::KindOf.new(
              resource_class: Netex::Route
            ).add(
              ::Netex::Validator::Rule::Attribute::Mandatory.new(
                name: 'direction_type'
              )
            )
          end

          def service_journey_pattern_rule
            ::Netex::Validator::RuleContext::Resource::KindOf.new(
              resource_class: Netex::ServiceJourneyPattern
            ).add(::Netex::Validator::Collection.new(collection: :points_in_sequence)
              .add(::Netex::Validator::Rule::Attribute::Mandatory.new(name: 'for_boarding'))
              .add(::Netex::Validator::Rule::Attribute::Mandatory.new(name: 'for_alighting')))
          end

          def line_operator_data_source_ref_rule
            ::Netex::Validator::RuleContext::Resource::KindOf.new(
              resource_classes: [Netex::Line, Netex::Operator]
            ).add(
              ::Netex::Validator::Rule::Attribute::Value.new(
                name: 'data_source_ref',
                value: 'FR1:OrganisationalUnit:IDFM:',
                tag: 'data_source_ref'
              )
            )
          end
        end
      end
    end
  end
end
