# frozen_string_literal: true

module Netex
  module Validator
    module RuleSet
      module IDFM
        class IBOO < Netex::Validator::RuleSet::Base
          def initialize # rubocop:disable Metrics/MethodLength
            super

            # CompositeFrame id="ACME:CompositeFrame:NETEX_OFFRE_LIGNE-1:LOC"

            organisation_pattern = '[^_]*'

            rules <<
              ::Netex::Validator::RuleContext::Resource::KindOf.new(
                resource_class: Netex::CompositeFrame
              ).add(
                ::Netex::Validator::RuleContext::Attribute::Match.new(
                  name: 'id',
                  value: 'NETEX_OFFRE_LIGNE'
                ).add(
                  ::Netex::Validator::Rule::Attribute::Match.new(
                    name: 'id',
                    value: /:CompositeFrame:NETEX_OFFRE_LIGNE-C\d+:LOC/
                  )
                ).add(
                  ::Netex::Validator::Rule::Element::Value.new(
                    name: 'type_of_frame',
                    value: 'FR1:TypeOfFrame:NETEX_OFFRE_LIGNE:'
                  )
                ).add(
                  ::Netex::Validator::Rule::Tag::Match.new(
                    name: 'filename',
                    value: %r{OFFRE_#{organisation_pattern}_20\d{12}Z/offre_C\d+_.*\.xml}
                  )
                )
              )

            rules <<
              ::Netex::Validator::RuleContext::Resource::KindOf.new(
                resource_class: Netex::GeneralFrame
              ).add(
                ::Netex::Validator::RuleContext::Attribute::Match.new(
                  name: 'id',
                  value: 'NETEX_STRUCTURE'
                ).add(
                  ::Netex::Validator::Rule::Attribute::Match.new(
                    name: 'id',
                    value: /:GeneralFrame:NETEX_STRUCTURE-.*:LOC/
                  )
                ).add(
                  ::Netex::Validator::Rule::Element::Value.new(
                    name: 'type_of_frame',
                    value: 'FR1:TypeOfFrame:NETEX_STRUCTURE:'
                  )
                )
              )

            rules <<
              ::Netex::Validator::RuleContext::Resource::KindOf.new(
                resource_classes: [
                  Netex::Route, Netex::Direction, Netex::ServiceJourneyPattern,
                  Netex::DestinationDisplay, Netex::ScheduledStopPoint, Netex::PassengerStopAssignment
                ]
              ).add(
                ::Netex::Validator::Rule::Tag::Match.new(
                  name: 'frame_id',
                  value: /GeneralFrame:NETEX_STRUCTURE/
                )
              )

            rules <<
              ::Netex::Validator::RuleContext::Resource::KindOf.new(
                resource_class: Netex::GeneralFrame
              ).add(
                ::Netex::Validator::RuleContext::Attribute::Match.new(
                  name: 'id',
                  value: 'NETEX_HORAIRE'
                ).add(
                  ::Netex::Validator::Rule::Attribute::Match.new(
                    name: 'id',
                    value: /:GeneralFrame:NETEX_HORAIRE-.*:LOC/
                  )
                ).add(
                  ::Netex::Validator::Rule::Element::Value.new(
                    name: 'type_of_frame',
                    value: 'FR1:TypeOfFrame:NETEX_HORAIRE:'
                  )
                )
              )

            rules <<
              ::Netex::Validator::RuleContext::Resource::KindOf.new(
                resource_classes: [
                  Netex::ServiceJourney
                ]
              ).add(
                ::Netex::Validator::Rule::Tag::Match.new(
                  name: 'frame_id',
                  value: /GeneralFrame:NETEX_HORAIRE/
                )
              )

            rules <<
              ::Netex::Validator::RuleContext::Resource::KindOf.new(
                resource_class: Netex::GeneralFrame
              ).add(
                ::Netex::Validator::RuleContext::Attribute::Match.new(
                  name: 'id',
                  value: 'NETEX_CALENDRIER'
                ).add(
                  ::Netex::Validator::Rule::Attribute::Match.new(
                    name: 'id',
                    value: /:GeneralFrame:NETEX_CALENDRIER-.*:LOC/
                  )
                ).add(
                  ::Netex::Validator::Rule::Element::Value.new(
                    name: 'type_of_frame',
                    value: 'FR1:TypeOfFrame:NETEX_CALENDRIER:'
                  )
                ).add(
                  ::Netex::Validator::Rule::Tag::Match.new(
                    name: 'filename',
                    value: %r{OFFRE_#{organisation_pattern}_20\d{12}Z/calendriers.xml}
                  )
                )
              )

            rules <<
              ::Netex::Validator::RuleContext::Resource::KindOf.new(
                resource_classes: [
                  Netex::DayType, Netex::DayTypeAssignment, Netex::OperatingPeriod
                ]
              ).add(
                ::Netex::Validator::Rule::Tag::Match.new(
                  name: 'frame_id',
                  value: /GeneralFrame:NETEX_CALENDRIER/
                )
              )

            %w[
              DayType DayTypeAssignment OperatingPeriod
              Route PointOnRoute Direction ServiceJourneyPattern DestinationDisplay ScheduledStopPoint PassengerStopAssignment
              ServiceJourney
            ].each do |resource_type|
              resource_class = Netex::Resource.for(resource_type)

              rules <<
                ::Netex::Validator::RuleContext::Resource::KindOf.new(
                  resource_class: resource_class
                ).add(
                  ::Netex::Validator::Rule::Attribute::Match.new(
                    name: 'id',
                    value: /:#{resource_type}:.*:LOC$/
                  )
                )
            end

            [Netex::DayType, Netex::Operator, Netex::ScheduledStopPoint, Netex::Direction, Netex::DestinationDisplay,
             Netex::Route].each do |resource_class|
              rules << ::Netex::Validator::Rule::Reference::Exists.new(referenced_class: resource_class)
            end

            rules <<
              ::Netex::Validator::RuleContext::Resource::KindOf.new(
                resource_classes: [
                  Netex::ServiceJourneyPattern
                ]
              ).add(
                ::Netex::Validator::Rule::Element::Value.new(
                  name: 'service_journey_pattern_type',
                  value: 'passenger'
                )
              )

          end
        end
      end
    end
  end
end
