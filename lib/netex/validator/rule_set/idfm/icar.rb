# frozen_string_literal: true

module Netex
  module Validator
    module RuleSet
      module IDFM
        class ICAR < Netex::Validator::RuleSet::Base
          def initialize # rubocop:disable Metrics/MethodLength
            super

            site_id_pattern = '\\d+'
            site_name_pattern = '[A-Z]+'
            time_pattern = '20\\d{6}T\\d{6}Z'

            rules <<
              ::Netex::Validator::RuleContext::Resource::KindOf.new(
                resource_class: Netex::GeneralFrame
              ).add(
                ::Netex::Validator::RuleContext::Attribute::Match.new(
                  name: 'id',
                  value: 'NETEX_ARRET_IDF'
                ).add(
                  ::Netex::Validator::Rule::Attribute::Match.new(
                    name: 'id',
                    value: /^#{site_name_pattern}:GeneralFrame:NETEX_ARRET_IDF_#{time_pattern}:LOC$/
                  )
                ).add(
                  ::Netex::Validator::Rule::Element::Value.new(
                    name: 'type_of_frame',
                    value: 'FR1:TypeOfFrame:NETEX_ARRET'
                  ) # TODO: check TypeOfFrame version :-/
                ).add(
                  ::Netex::Validator::Rule::Tag::Match.new(
                    name: 'filename',
                    value: /ARRET_#{site_id_pattern}_#{site_name_pattern}_T_#{time_pattern}.xml/
                  )
                ).add(
                  ::Netex::Validator::Rule::Attribute::Value.new(
                    name: 'modification',
                    value: 'revise'
                  )
                )
              )

            rules <<
              ::Netex::Validator::RuleContext::Resource::KindOf.new(
                resource_class: Netex::Quay
              ).add(
                ::Netex::Validator::Rule::Tag::Match.new(
                  name: 'frame_id',
                  value: /GeneralFrame:NETEX_ARRET_IDF/
                )
              ).add(
                ::Netex::Validator::Rule::Attribute::Mandatory.new(
                  name: 'changed'
                )
              ).add(
                ::Netex::Validator::Rule::Attribute::Match.new(
                  name: 'id',
                  value: /FR::Quay:5\d{7}:FR1/
                )
              ).add(
                ::Netex::Validator::Rule::Element::Mandatory.new(
                  name: 'name'
                )
              ).add(
                ::Netex::Validator::Rule::Element::Mandatory.new(
                  name: 'private_code'
                )
              ).add(
                ::Netex::Validator::Rule::Element::Mandatory.new(
                  name: 'transport_mode'
                )
              )
          end
        end
      end
    end
  end
end
