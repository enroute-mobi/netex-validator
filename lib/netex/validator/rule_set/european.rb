# frozen_string_literal: true

module Netex
  module Validator
    module RuleSet
      class European < Base
        def initialize
          super

          rules << ::Netex::Validator::Rule::Reference::Exist.new(
            referenced_class: Netex::Operator
          )
        end
      end
    end
  end
end
