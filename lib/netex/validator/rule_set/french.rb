# frozen_string_literal: true

module Netex
  module Validator
    module RuleSet
      class French < Base
        def initialize
          super

          # TODO: PublicationDelivery .. are ignored by NETEx::Source
          # rules << RuleContext::Resource::KindOf.new(
          #   resource_class: Netex::PublicationDelivery
          # ).add(Rule::Attribute::Match.new(name: "version", value: "1.09:FR-NETEX-2.1-1.0"))

          rules << RuleContext::Resource::KindOf.new(
            resource_classes: [Netex::Quay, Netex::StopPlace]
          ).add(Rule::Attribute::Match.new(name: 'version', value: 'any', code: 'version-any'))

          [
            Netex::Quay, Netex::StopPlace, Netex::Line, Netex::DayType,
            Netex::Operator, Netex::ScheduledStopPoint, Netex::Direction,
            Netex::DestinationDisplay, Netex::Route, Netex::RoutePoint,
            Netex::JourneyPattern, Netex::OperatingPeriod
          ].each do |resource_class|
            rules << ::Netex::Validator::Rule::Reference::Exist.new(referenced_class: resource_class, code: 'unknown-reference')
          end

          rules << RuleContext::Resource::KindOf.new(
            resource_classes: [Netex::Quay, Netex::StopPlace]
          ).add(
            ::Netex::Validator::Rule::Attribute::Mandatory.new(name: 'latitude', code: 'latitude-mandatory')
          ).add(
            ::Netex::Validator::Rule::Attribute::Mandatory.new(name: 'longitude', code: 'longitude-mandatory')
          )

          rules << RuleContext::Resource::KindOf.new(
            resource_class: Netex::OperatingPeriod
          ).add(
            ::Netex::Validator::Rule::Resource::Class.new(expected_class: Netex::UicOperatingPeriod, code: 'uic-operating-period')
          ).add(
            ::Netex::Validator::Rule::Attribute::Mandatory.new(name: 'valid_day_bits', code: 'valid-day-bits')
          )

          rules <<
              ::Netex::Validator::RuleContext::Resource::KindOf.new(
                resource_class: Netex::CompositeFrame
              ).add(
                ::Netex::Validator::RuleContext::Attribute::Match.new(
                  name: 'id',
                  value: 'FR:CompositeFrame:NETEX_LIGNE:LOC'
                ).add(
                  ::Netex::Validator::Rule::Element::Value.new(
                    name: 'type_of_frame',
                    value: 'FR:TypeOfFrame:NETEX_LIGNE:',
                    code: 'composite-frame-ligne-type-of-frame'
                  ) # TODO: check TypeOfFrame version :-/
                ).add(
                  ::Netex::Validator::Rule::Tag::Match.new(
                    name: 'filename',
                    value: /^LINE-.*.xml$/,
                    code: 'composite-frame-ligne-filename'
                  )
                # ).add(
                #   ::Netex::Validator::Rule::Attribute::Mandatory.new(
                #     name: 'name'
                #   )
                ).add(
                  ::Netex::Validator::Rule::Resource::Mandatory.new(code: 'composite-frame-ligne-mandatory')
                )
              )

          rules <<
              ::Netex::Validator::RuleContext::Resource::KindOf.new(
                resource_class: Netex::GeneralFrame
              ).add(
                ::Netex::Validator::RuleContext::Attribute::Match.new(
                  name: 'id',
                  value: 'FR:GeneralFrame:NETEX_LIGNE:LOC'
                ).add(
                  ::Netex::Validator::Rule::Element::Value.new(
                    name: 'type_of_frame',
                    value: 'FR:TypeOfFrame:NETEX_LIGNE:',
                    code: 'general-frame-ligne-type-of-frame'
                  ) # TODO: check TypeOfFrame version :-/
                ).add(
                  ::Netex::Validator::Rule::Tag::Match.new(
                    name: 'filename',
                    value: /^LINE-.*.xml$/,
                    code: 'general-frame-ligne-filename'
                  )
                )
              )

          rules <<
              ::Netex::Validator::RuleContext::Resource::KindOf.new(
                resource_class: Netex::GeneralFrame
              ).add(
                ::Netex::Validator::RuleContext::Attribute::Match.new(
                  name: 'id',
                  value: 'FR:GeneralFrame:NETEX_HORAIRE:LOC'
                ).add(
                  ::Netex::Validator::Rule::Element::Value.new(
                    name: 'type_of_frame',
                    value: 'FR:TypeOfFrame:NETEX_HORAIRE:',
                    code: 'general-frame-horaire-type-of-frame'
                  ) # TODO: check TypeOfFrame version :-/
                ).add(
                  ::Netex::Validator::Rule::Tag::Match.new(
                    name: 'filename',
                    value: /^LINE-.*.xml$/,
                    code: 'general-frame-horaire-filename'
                  )
                )
              )

          rules <<
              ::Netex::Validator::RuleContext::Resource::KindOf.new(
                resource_class: Netex::GeneralFrame
              ).add(
                ::Netex::Validator::RuleContext::Attribute::Match.new(
                  name: 'id',
                  value: 'FR:GeneralFrame:NETEX_RESEAU:LOC'
                ).add(
                  ::Netex::Validator::Rule::Element::Value.new(
                    name: 'type_of_frame',
                    value: 'FR:TypeOfFrame:NETEX_RESEAU:',
                    code: 'general-frame-reseau-type-of-frame'
                  ) # TODO: check TypeOfFrame version :-/
                ).add(
                  ::Netex::Validator::Rule::Tag::Match.new(
                    name: 'filename',
                    value: /^LINE-.*.xml$/,
                    code: 'general-frame-reseau-filename'
                  )
                )
              )

          rules <<
              ::Netex::Validator::RuleContext::Resource::KindOf.new(
                resource_class: Netex::GeneralFrame
              ).add(
                ::Netex::Validator::RuleContext::Attribute::Match.new(
                  name: 'id',
                  value: 'FR:GeneralFrame:NETEX_ARRET:LOC'
                ).add(
                  ::Netex::Validator::Rule::Element::Value.new(
                    name: 'type_of_frame',
                    value: 'FR:TypeOfFrame:NETEX_ARRET:',
                    code: 'general-frame-arret-type-of-frame'
                  ) # TODO: check TypeOfFrame version :-/
                ).add(
                  ::Netex::Validator::Rule::Tag::Match.new(
                    name: 'filename',
                    value: /^STOP.xml$/,
                    code: 'general-frame-arret-filename'
                  )
                )
              )

          rules <<
            ::Netex::Validator::RuleContext::Resource::KindOf.new(
              resource_classes: [
                Netex::Line, Netex::Direction, Netex::Route, Netex::RoutePoint,
                Netex::PointOnRoute
              ]
            ).add(
              ::Netex::Validator::Rule::Tag::Match.new(
                name: 'frame_id',
                value: 'FR:GeneralFrame:NETEX_LIGNE:LOC',
                code: 'frame-ligne-resources'
              )
            )

          rules <<
            ::Netex::Validator::RuleContext::Resource::KindOf.new(
              resource_classes: [Netex::ServiceJourney]
            ).add(
              ::Netex::Validator::Rule::Tag::Match.new(
                name: 'frame_id',
                value: 'FR:GeneralFrame:NETEX_HORAIRE:LOC',
                code: 'frame-horaire-resources'
              )
            )

          rules <<
            ::Netex::Validator::RuleContext::Resource::KindOf.new(
              resource_classes: [
                Netex::DestinationDisplay
              ]
            ).add(
              ::Netex::Validator::Rule::Tag::Match.new(
                name: 'frame_id',
                value: 'FR:GeneralFrame:NETEX_RESEAU:LOC',
                code: 'frame-reseau-resources'
              )
            )

          rules <<
            ::Netex::Validator::RuleContext::Resource::KindOf.new(
              resource_classes: [
                Netex::StopPlace, Netex::Quay, Netex::StopPlaceEntrance
              ]
            ).add(
              ::Netex::Validator::Rule::Tag::Match.new(
                name: 'frame_id',
                value: 'FR:GeneralFrame:NETEX_ARRET:LOC',
                code: 'frame-arret-resources'
              )
            )

          rules <<
            ::Netex::Validator::RuleContext::Resource::KindOf.new(
              resource_classes: [
                Netex::PointOfInterest
              ]
            ).add(
              ::Netex::Validator::Rule::Tag::Match.new(
                name: 'frame_id',
                value: 'FR:GeneralFrame:NETEX_COMMUN:LOC',
                code: 'frame-commun-resources'
              )
            )

          rules <<
            ::Netex::Validator::RuleContext::Resource::KindOf.new(
              resource_classes: [
                Netex::DayType, Netex::DayTypeAssignment, Netex::OperatingPeriod
              ]
            ).add(
              ::Netex::Validator::Rule::Tag::Match.new(
                name: 'frame_id',
                value: 'FR:GeneralFrame:NETEX_CALENDRIER:LOC',
                code: 'frame-calendrier-resources'
              )
            )
        end
      end
    end
  end
end
