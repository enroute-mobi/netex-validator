# frozen_string_literal: true

module Netex
  module Validator
    module Description
      class Memory
        def contextes
          @contextes ||= []
        end

        attr_reader :description, :status

        def context(description)
          Memory.new.tap do |d|
            d.describe description
            contextes << d
          end
        end

        def describe(description, status: nil, code: nil)
          @description = description
          @status = status
          @code = code
        end

        def messages
          @messages ||= []
        end

        def message(message)
          messages << message
        end
      end

      class Output
        def initialize(output: nil, indent: 0, &block)
          @output = if block_given?
                      block
                    else
                      output || proc { |message| puts message }
                    end

          @indent = indent
        end

        attr_reader :indent, :output

        def context(description)
          describe description
          more_indented
        end

        def more_indented
          Output.new output: output, indent: indent + 1
        end

        def output_indent
          @output_indent ||= '  ' * indent
        end

        def describe(description, status: nil, code: nil)
          output_status =
            case status
            when :success
              '✓'
            when :error
              '✗'
            when :warning
              '!'
            when :skipped
              '?'
            else
              '*'
            end
          code_part = " (#{code})" if code
          output.call "#{output_indent}#{output_status} #{description}#{code_part}"
        end

        def message(message)
          output.call "#{output_indent}  - #{message.full_message}"
        end
      end
    end
  end
end
