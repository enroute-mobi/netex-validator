# frozen_string_literal: true

module Netex
  module Validator
    module Ruler
      def rules
        @rules ||= []
      end

      def empty?
        rules.empty?
      end

      def visit(resource, options = {})
        rules.each do |rule|
          rule.visit resource, options if rule.match?(resource)
        end
      end

      def finalize
        rules.each(&:finalize)
      end

      def describe(description)
        rules.each { |rule| rule.describe(description) }
      end

      def each_message(&block)
        rules.each { |rule| rule.each_message(&block) }
      end

      def messages
        Messages.new self
      end

      class Messages < Enumerator
        attr_reader :ruler

        def initialize(ruler)
          super(nil) do |yielder|
            ruler.each_message do |message|
              yielder << message
            end
          end
          @ruler = ruler
        end

        def empty?
          ruler.rules.each do |rule|
            rule.each_message do
              return false
            end
          end

          true
        end

        def inspect
          "#<Netex::Validator::Checker::Messages: #{count} messages>"
        end
      end
    end

    module MutableRuler
      include Ruler

      def add(rule)
        rules << rule
        self
      end
      alias << add
    end
  end
end
