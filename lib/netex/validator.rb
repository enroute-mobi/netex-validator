# frozen_string_literal: true

require 'netex'
require 'open-uri'
require 'set'
require 'benchmark'
require 'json'
require 'cuckoo'
require 'active_support/core_ext/string/inflections'

require_relative 'validator/version'

module Netex
  module Validator
  end
end

require_relative 'validator/ruler'
require_relative 'validator/rule_context'
require_relative 'validator/collection'
require_relative 'validator/rule'
require_relative 'validator/rule_set'
require_relative 'validator/rule_set/european'
require_relative 'validator/rule_set/french'
require_relative 'validator/rule_set/idfm'
require_relative 'validator/rule_set/idfm/full'
require_relative 'validator/rule_set/idfm/iboo'
require_relative 'validator/rule_set/idfm/icar'
require_relative 'validator/rule_set/none'
require_relative 'validator/checker'
require_relative 'validator/description'
require_relative 'validator/rule_set/loader'
require_relative 'validator/base'
